import xml.etree.ElementTree as elmTree
import httplib2
from datetime import datetime, date, time
import CtaData as CtaData

import xml.dom.minidom

class CtaArrivalsWrapper:
	
	__apiKey = '64591826749143ff9b1379319906dc59'
	__baseUrl = "http://lapi.transitchicago.com/api/1.0/ttarrivals.aspx?"


	def __init__(self, station):
		self.station = station		


	def GetNextNorthbound(self, count=0):
		return self.GetNextNTrain(self.station.North, count)		


	def GetNextSouthbound(self, count=0):
		return self.GetNextNTrain(self.station.South, count)


	def GetNextNTrain(self, direction, count=0):
		data = self.__GetTrainData(self.station.Id, direction)
		if count == 0: 
			count = len(data)
		index = min(count, len(data))
		data.sort(key=lambda x: x.Eta)
		return data[:index]


	def __GetTrainData(self, station, direction):
		httpLib = httplib2.Http(".cache")
		resp, content = httpLib.request(self.__BuildUrl(station,direction))
		root = elmTree.fromstring(content)

		data = []
		for child in root:
			line = self.__GetLine(child)
			arrT = self.__GetNextArrival(child) or datetime.now().strftime('%Y%m%d %H:%M:%S')
			prDt = self.__GetNextArrivalGenerationTime(child) or datetime.now().strftime('%Y%m%d %H:%M:%S')
			heading = self.__GetDirection(child)

			# if arrival time is now then prediction is useless
			if prDt == arrT: continue

			ctaData = CtaData.CtaData()
			ctaData.Line = line
			ctaData.Direction = heading
			ctaData.SetEta(arrT)
			ctaData.SetPrediction(prDt)

			data.append(ctaData)

		data.sort(key=lambda x: x.Eta)
		return data


	def __GetLine(self, element):
		rt = element.find('rt')
		return '' if rt is None else rt.text


	def __GetDirection(self, element):
		e = element.find('trDr')
		return 'N' if e is not None and e.text == '1' else 'S'


	def __GetNextArrival(self, element):
		return self.__GetCtaTime(element, 'arrT')


	def __GetNextArrivalGenerationTime(self, element):
		return self.__GetCtaTime(element, 'prdt')


	def __GetCtaTime(self, element, tag):
		timeElement = element.find(tag)
		return timeElement.text if timeElement is not None else ''


	def __BuildUrl(self, station, direction):
		return self.__baseUrl + 'key=' + self.__apiKey + '&mapid=' + str(station) +'&stpid=' + str(direction) 


