from datetime import datetime, date, time


class CtaData:
	
	
	def __init__(self):
		self.Line = ''
		self.Direction = ''
		self.Prediction = ''
		self.Station = ''
		self.Eta = ''

	def GetDelta(self):
		arrTime = datetime.strptime(self.Eta, '%Y%m%d %H:%M:%S') 
		predictTime = datetime.strptime(self.Prediction, '%Y%m%d %H:%M:%S')
		return arrTime - predictTime

	def SetEta(self, dtime):
		self.Eta = dtime

	def SetPrediction(self, dtime):
		self.Prediction = dtime

	def PrintDetail(self):
		print self.Line + ' ' + self.Direction + ' ' + str(self.GetDelta())
