#!/usr/bin/python
import xml.dom.minidom as xmlDom


class LocationData:
	def __init__(self, time, direction, line, content):
		self.time = time
		self.direction = direction
		self.line = line
		self.content = content

	def printData(self):
		print(self.time + " " + self.direction + " " + self.line)

	def printXml(self):
		print(xmlDom.parseString(self.content).toprettyxml())