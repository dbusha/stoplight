
class Station(object):

	def __init__(self, name, ident, north, south):
		self.Name =  name
		self.Id = ident
		self.North = north
		self.South = south


class Stations:
	Berwyn = 40340
	Morse = 40100
	Belmont = 41320

	StopNameToId = {
		'None': Station('None', 0, 0, 0),
		'Berwyn': Station('Berwyn', 40340, 30066, 30067), 
		'Morse': Station('Morse', 40100, 30020, 30021)

		}

	StopIdToName = {40340: 'Berwyn', 40100: 'Morse'}