from CtaArrivalsWrapper import CtaArrivalsWrapper
from Stations import Stations
import Tkinter as tk
import datetime
import threading
import time


class Application(tk.Frame):

    def __init__(self, master=None):
        self.master = master
        self.isStopped = False
        tk.Frame.__init__(self, master)
        self.pack()
        self.createWidgets(master)
        self.timer = threading.Timer(30, self.__RunImpl)
        self.__RunImpl()
        self.timer.start()


    def createWidgets(self, master):
        self.cmbValue = tk.StringVar(master)
        self.cmbValue.set(Stations.StopIdToName[Stations.Morse])
        self.cmb = tk.OptionMenu(master, 
            self.cmbValue, 
            Stations.StopIdToName[Stations.Berwyn], 
            Stations.StopIdToName[Stations.Morse])
        self.cmbValue.trace_variable('w', self.__OnComboChanged)

        self.cmb.pack()
        self.t1, self.t1TextIndex = self.CreateCanvas()
        self.t2, self.t2TextIndex = self.CreateCanvas()
        self.QuitBtn = tk.Button(self, text="Exit", command=self.__Stop)
        self.QuitBtn.pack()


    def CreateCanvas(self):
        canvas = tk.Canvas()
        canvas.config(height=50, width=150, bg="blue")
        canvas.pack(side="top")
        index = canvas.create_text(30,30,text='test')
        return (canvas, index)


    def __RunImpl(self):
        print "running..."
        stationId = Stations.StopNameToId[self.cmbValue.get()]
        wrapper = CtaArrivalsWrapper(stationId)
        trains = wrapper.GetNextSouthbound()

        self.t1.config(bg="red")
        self.t2.config(bg="red")
        self.t1.itemconfig(self.t1TextIndex, text=trains[0].GetDelta())
        self.t2.itemconfig(self.t2TextIndex, text=trains[1].GetDelta())

        if len(trains) == 1:
            self.t1.config(bg=self.__GetColorForTime(trains[0].GetDelta()))
        elif len(trains) > 1:
            t1 = trains[0].GetDelta()
            t2 = trains[1].GetDelta()
            self.t1.config(bg=self.__GetColorForTime(t1))
            self.t2.config(bg=self.__GetColorForTime(t2))


    def __Stop(self):
        self.isStopped = True
        self.master.destroy()


    def __GetColorForTime(self, time):
        minutes = datetime.timedelta.total_seconds(time)/60
        if minutes >= 5: return "green"
        if minutes >= 3: return "orange"
        return "red"

    def __OnComboChanged(self, name, index, mode):
        print "combo changed"
        self.__RunImpl()


